import cv2
import numpy as np

def enhance_follicles(img):
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    clahe_img = clahe.apply(img)

    equalized_img = cv2.equalizeHist(clahe_img)

    return equalized_img

image_path = './data/train/infected/img_0_698.jpg'
original_img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

enhanced_img = enhance_follicles(original_img)

filtered_img = cv2.medianBlur(enhanced_img, 5)
filtered_img = cv2.GaussianBlur(filtered_img, (5, 5), 0)

cv2.imshow('Original Image', original_img)
cv2.imshow('Enhanced Image using merge', filtered_img)

cv2.waitKey(0)
cv2.destroyAllWindows()
