# FEMICHECK

## An Automatic PCOS Detection System

This is an automatic PCOS detection system built using deep learning and medical image analysis techniques. The system aims to accurately identify 
 potential cases of PCOS based on relevant diagnostic data and imaging results. By automating the detection process, we aim to improve the efficiency of diagnosis, reduce human errors, and enable early intervention to improve the quality of life for affected individuals.

## Acknowledgement

The PCOS detection system was inspired by the need to make a positive impact on healthcare with improved diagnostic capabilities and early intervention for individuals affected by PCOS.

## Documentation

Here's the [Internal Documentation](https://docs.google.com/document/d/1PuphqVf6v1EpK_nXWnGuk4x1d52sppeSWtUSTYfJsAg/edit) and [Research Documentation](https://docs.google.com/document/d/1N1F65nohXwS0LuxUvkuk0ydKAwk9Ia9Fjh-f5Ej3jtg/edit) to keep a track of all progress, highlight the contribution of members in the research work and meet the set deadlines of the project.

## Tech Stack

* Design : Figma ( for collaborative UI / UX )
* Version Control: Git / GitLab
* Frontend : HTML, CSS, JavaScript, ReactJS
* Backend : Python , Flask
* Database : MongoDB
* YOLOv8 dependencies

## Authors

- [Radhika Amar Desai](https://gitlab.com/radhikaamar.desai2022)
- [Radhika Garg](https://gitlab.com/22ucc082)
- [Vaishnavi Singh](https://gitlab.com/eyevaishnavisingh)
- [Pragati Das](https://gitlab.com/daspragati)
- [Riya Kachhara](https://gitlab.com/Riya_kachhara)

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are greatly appreciated.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement". Don't forget to give the project a star! Thanks again!

1. Clone the Project
2. Create your Feature Branch (git checkout -b feature/NewFeature)
3. Commit your Changes (git commit -m 'Add some NewFeature')
4. Push to the Branch (git push origin feature/NewFeature)
5. Open a Pull Request

# License

Unissued