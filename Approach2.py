import numpy as np
import cv2
from skimage import io
from matplotlib import pyplot as plt

img = cv2.imread("img_0_9909.jpg", 1)
lab_img= cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
l, a, b = cv2.split(lab_img)
equ = cv2.equalizeHist(l)
updated_lab_img1 = cv2.merge((equ,a,b))

hist_eq_img = cv2.cvtColor(updated_lab_img1, cv2.COLOR_LAB2BGR)

clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8,8))
clahe_img = clahe.apply(l)

def otsu_thresholding(image):
    hist, bins = np.histogram(image.flatten(), 256, [0, 256])
    total_pixels = image.shape[0] * image.shape[1]

    probabilities = hist / total_pixels

    best_threshold = 0
    max_variance = 0

    for threshold in range(256):

        prob_background = np.sum(probabilities[:threshold])
        prob_foreground = 1 - prob_background

        mean_background = np.sum(np.arange(threshold) * probabilities[:threshold]) / prob_background
        mean_foreground = np.sum(np.arange(threshold, 256) * probabilities[threshold:]) / prob_foreground

        variance = prob_background * prob_foreground * (mean_background - mean_foreground) ** 2
        
        if variance > max_variance:
            max_variance = variance
            best_threshold = threshold

    
    binary_image = np.where(image >= best_threshold, 255, 0).astype(np.uint8)

    return binary_image
def image_complement(image):
    max_intensity = 255
    complemented_image = max_intensity - image
    return complemented_image

def erode_image(image, kernel_size=(3, 3), iterations=1):
    # Create a kernel for the erosion operation
    kernel = np.ones(kernel_size, np.uint8)

    # Perform the erosion operation
    eroded_image = cv2.erode(complemented_image, kernel, iterations=iterations)

    return eroded_image

kernel_size = (5, 5)  
iterations = 1        

updated_lab_img2 = cv2.merge((clahe_img,a,b))

CLAHE_img = cv2.cvtColor(updated_lab_img2, cv2.COLOR_LAB2BGR)
binary_image = otsu_thresholding(clahe_img)
complemented_image = image_complement(binary_image)
eroded_image = erode_image(binary_image, kernel_size, iterations)   
_, binary_image = cv2.threshold(complemented_image, 128, 255, cv2.THRESH_BINARY)
cv2.imshow("Original image", img)
cv2.imshow("Equalized image", hist_eq_img)
cv2.imshow('CLAHE Image', CLAHE_img)
cv2.imshow("Binary Image (Otsu)", binary_image)
cv2.imshow("Complemented Image", complemented_image)
cv2.imshow("Eroded Image", eroded_image)
cv2.waitKey(0)
cv2.destroyAllWindows() 