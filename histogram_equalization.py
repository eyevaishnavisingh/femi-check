import cv2
import numpy as np

def equalization_of_image (image):
    
    return cv2.equalizeHist(image)

image_path = "/home/radhika/Downloads/archive(5)/data/test/infected/img_0_497.jpg"
image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

equalized_image = equalization_of_image (image)

cv2.imshow('Original Image', image)
cv2.imshow ('Histogram equalization',equalized_image)

cv2.waitKey(0)
cv2.destroyAllWindows()
