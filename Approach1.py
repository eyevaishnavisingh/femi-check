import cv2
import matplotlib.pyplot as plt
import numpy as np

def apply_median_filter(image, kernel_size=3):
    # Apply Median filter to the image
    filtered_image = cv2.medianBlur(image, kernel_size)
    return filtered_image

def apply_gaussian_filter(image, kernel_size=(5, 5), sigma_x=0):
    # Apply Gaussian filter to the image
    filtered_image = cv2.GaussianBlur(image, kernel_size, sigma_x)
    return filtered_image

image_path = 'img_0_60.jpg'
image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

if image is None:
    print("Error: Image not found or invalid format.")
else:
    median_filtered_image = apply_median_filter(image)
    gaussian_filtered_image = apply_gaussian_filter(median_filtered_image)

    cv2.imshow("Original Image", image)
    cv2.imshow("Median Filtered Image", median_filtered_image)
    cv2.imshow("Gaussian Filtered Image", gaussian_filtered_image)

#Contrast Enhancement using adaptive
def apply_adaptive_histogram_equalization(image, clip_limit=2.0, grid_size=(8, 8)):
    clahe = cv2.createCLAHE(clipLimit=clip_limit, tileGridSize=grid_size)
    equalized_image = clahe.apply(image)
    return equalized_image
    
equalized_image = apply_adaptive_histogram_equalization(image)
cv2.imshow("Adaptive", equalized_image)

#Edge Detection
def apply_canny_edge_detection(image, min_threshold, max_threshold):
    edges = cv2.Canny(image, min_threshold, max_threshold)
    return edges
min_threshold = 100
max_threshold = 200
edges = apply_canny_edge_detection(equalized_image, min_threshold, max_threshold)
cv2.imshow("Canny edge", edges)

#Speckle Reduction
def apply_non_local_means_filter(image, h=10, sigma_color=10, sigma_space=10):
    denoised_image = cv2.fastNlMeansDenoising(image, None, h, sigma_color, sigma_space)
    return denoised_image

h = 10             
sigma_color = 10    
sigma_space = 10    
denoised_image = apply_non_local_means_filter(edges, h, sigma_color, sigma_space)
cv2.imshow("Denoised Image (Non-Local Means Filter)", denoised_image)

#Thresholding and Segmentation
def apply_adaptive_thresholding(image, block_size=11, c=2):
    binary_image = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                         cv2.THRESH_BINARY, block_size, c)
    return binary_image
block_size = 11  
c = 2            
binary_image = apply_adaptive_thresholding(denoised_image, block_size, c)
cv2.imshow("Thresholded Image", binary_image)
cv2.waitKey(0)
cv2.destroyAllWindows()