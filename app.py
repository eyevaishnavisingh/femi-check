from flask import Flask, render_template, request, redirect, url_for
from roboflow import Roboflow
import cv2
from io import BytesIO
import numpy as np
import base64
import os

app = Flask(__name__)

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def encode_image(file):
    return cv2.imdecode(np.fromstring(file.read(), np.uint8), cv2.IMREAD_COLOR)

def save_image_to_folder(image,label):
    folder_path = "static/Images"
    processed_image_path = os.path.join(folder_path, label +'.jpg')
    cv2.imwrite(processed_image_path, image)

def yolo_predictions(image_path,predictions_path):
    rf = Roboflow(api_key="B5mjxnXFSCXh1GUWUR8O")
    project = rf.workspace().project("pcos-detection-yxo7e")
    model = project.version(1).model
    # visualize your prediction
    model.predict(image_path, confidence=40, overlap=30).save(predictions_path)
    return predictions_path

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/process', methods=['POST'])
def process():
    if 'image1' not in request.files or 'image2' not in request.files:
        return redirect(request.url)

    file1 = request.files['image1']
    file2 = request.files['image2']

    if file1.filename == '' or file2.filename == '':
        return redirect(request.url)

    # Process the images (apply blur)
    image1_folder = encode_image(file1)
    image2_folder = encode_image(file2)

    save_image_to_folder ( image1_folder, "left_ovary" )
    save_image_to_folder ( image2_folder, "right_ovary" )

    yolo_predictions ( "static/Images/left_ovary.jpg", "static/predictions/left_ovary.jpg" )
    yolo_predictions ( "static/Images/right_ovary.jpg", "static/predictions/right_ovary.jpg" )

    return render_template('result.html')

if __name__ == '__main__':
    app.run(debug=True)
